# Introduction

ESP8266 Project that reads the Dutch Smart Meter P1 port via HW UART and stores it in INFLUX DB 

## P1 Port
- Note that the RS232 output by P1 port is inverted and therefore there is the need to create a supporting PCB.
## Hardware
- ESP8266 board: WeMos_D1_mini

### Schematic
![](./resources/pcb_schematic.png)
